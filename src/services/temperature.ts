import http from "./http"

type Returndata = {
    celsius: number
    fahrenheit: number
}
async function convert(celsius: number): Promise<number> {
    console.log('Service: call convert')
    console.log(`/temperature/convert/${celsius}`)
    // ถ้าเป็น Post it will be sent with json style writing but get query or param it will be like line upper
    // await ของ axios อ่านว่า แอ็กซีออส จะทำงานแบบ Asynchronous คือ แบบไปแล้วตัวให้เสร็จก่อนให้รันไปก่อน แล้วใช้await ข้างหน้าด้วย บน function จะมี async
    // axios. ตามด้วยชื่อ operation get post ตามด้วย http และรูปแบบของเขา
    const res = await http.post(`/temperature/convert`, { celsius: celsius })
    const convertResult = res.data as Returndata
    console.log('Service: finish call convert')
    return convertResult.fahrenheit
}
export default { convert }